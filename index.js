//display the meals
let mealContainer = document.getElementById("mealContainer");
mealContainer.innerHTML = `<h2 class="text-center mb-3">
قائمة الطعام
</h2>`;

fetch("http://127.0.0.1:8000/api/meals", {
    method: "GET",
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    },
})
    .then((res) => {
        return Promise.all([res.json(), res.status])
    })
    .then(([response, status]) => {
        
        for (let i = 0; i < parseInt(response.meals.length/3); i++) {
            let html=`<div class="row text-center">
            <div class="col-sm">
                <div class="card bg-dark text-light mb-3">
                    <img src="`+response.meals[i*3].logo+`" alt="" class="card-img-top">

                    <div class="card-body text-center">

                        <h4 class="card-title text-success">`+response.meals[i*3].name+`</h4>
                        <div class="card-text">`+response.meals[i*3].price+` sp</div>
                      
                        
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card bg-dark text-light mb-3">
                    <img src="`+response.meals[i*3+1].logo+`" alt="" class="card-img-top">
                    <div class="card-body text-center">
                        <h4 class="card-title text-success">`+response.meals[i*3+1].name+`</h4>
                        <div class="card-text">`+response.meals[i*3+1].price+` sp</div>
                     
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card bg-dark text-light mb-3">
                    <img src="`+response.meals[i*3+2].logo+`" alt="" class="card-img-top">
                    <div class="card-body text-center">
                        <h4 class="card-title text-success">`+response.meals[i*3+2].name+`</h4>
                        <div class="card-text">`+response.meals[i*3+2].price+` sp</div>
                      
                    </div>
                </div>
            </div>
        </div>`;

        mealContainer.innerHTML+=html;  
      }

      //deal with the rest meals
      if(response.meals.length%3!=0){
        html=``;
        for(let i=parseInt(response.meals.length/3)*3; i<response.meals.length; i++){
            html+=`<div class="col-sm">
            <div class="card bg-dark text-light mb-3">
                <img src="`+response.meals[i].logo+`" alt="" class="card-img-top">
                <div class="card-body text-center">
                    <h4 class="card-title text-success">`+response.meals[i].name+`</h4>
                    <div class="card-text">`+response.meals[i].price+` sp</div>
                  
                </div>
            </div>
        </div>`;
        }
        
        mealContainer.innerHTML+=`<div class="row text-center">`+html+`</div>`;

    }
        
    });


