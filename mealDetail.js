let mealContainer = document.getElementById("mealContainer");

fetch("http://127.0.0.1:8000/api/meal/" + localStorage.getItem("mealID"), {
    method: "GET",
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    },
})
    .then((res) => {
        return Promise.all([res.json(), res.status])
    })
    .then(([response, status]) => {
        let components=response.meal.details.split(", ");
        console.log(components);
        let html=`<div class="card bg-dark text-light mb-3">
        <div class="container">
        <img src="`+response.meal.logo+`" alt="" class="card-img-top img-responsive" width="10" height="550">
    </div>
        <div class="card-body text-center">

            <h4 class="card-title text-success">`+response.meal.name+`</h4>
            <p>المكونات:</p>
            <ol>`;
            
            components.forEach(component => {
                html+=`<li>`+component+`</li>`
            });

            html+=`</ol>
            <div class="card-text">`+response.meal.price+` sp</div>
            <h4>الكمية:</h4><input type="number" />
            <hr/>
            <a href="pizza.html" class="btn btn-info"> اطلب الان</a>
            
        </div>
    </div>`;

    mealContainer.innerHTML=html;
    });